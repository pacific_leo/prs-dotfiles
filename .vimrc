" pratn :- Most of the things below are taken from various sources



" alexs neato vimrc

" i mostly stole this stuff from these sites:

" http://www.cip.biologie.uni-osnabrueck.de/niehaus/linux/howtos/img/vimrc
" http://vim.sourceforge.net/tips/tip.php?tip_id=156
" http://metacosm.dhs.org/
" http://www.csclub.uwaterloo.ca/u/mvcorks/vim/vimrc
" http://sites.netscape.net/bbenjif/vim/_vimrc
" http://www.naglenet.org/vim/syntax/_vimrc

" this sets up some non-sucky syntax coloring. dark blue on black
" is super hard to read.
highlight perlComment ctermfg=white
highlight perlComment ctermbg=blue
highlight rubyComment ctermfg=white
highlight rubyComment ctermbg=blue
highlight vimComment ctermfg=white
highlight vimComment ctermbg=blue

" this turns syntax on. duh.
syntax on

set nocompatible
set number
"set tabstop=2
set showmatch
set mat=2
set showmode
set nohlsearch
" set foldenable
" set foldmethod=marker
set backspace=indent,eol,start
set history=1024
set ruler
set hid
set viminfo='20,\"50
set backspace=2 "whichwrap+=<,>,h,l
set nocindent
set cmdheight=2
set comments=b:#,b:\",n:>
set ignorecase
set magic
set showmode

" fix these perl typos
ab prnt print
ab wran warn
ab prnit print

" substitute these typos for what I DWIM'd
cabbrev Wq wq
cabbrev Prel perl
cabbrev prel perl
cabbrev Perl perl
cabbrev pelr perl
cabbrev peerl perl

" like :wq except write and suspend
command Wst w <bar> st
cabbrev wst Wst

" add a nifty macro
ab ubp #!/usr/bin/perl
ab uws use warnings;
" use strict;

" this puts our search term in the middle of the screen
nmap n nmzz.`z
nmap N Nmzz.`z
nmap * *mzz.`z
nmap # #mzz.`z
nmap g* g*mzz.`z
nmap g# g#mzz.`z

" set expandtab
"set tw=80
" set ts=4
set sw=4
set tags=v:/wtl/occam/tags
set incsearch
set hlsearch
set cindent
"set guifont=courier_new:h10:cANSI
set guifont=Dina:h10:cANSI
set guifont=Courier\ New\ 12
"win 100 40

set viminfo='10,\"100,:20,%,n~/.viminfo
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif

set softtabstop=4    " Use soft tab stops mod 4
set shiftwidth=4     " Number of spaces for autoindent and >>, etc
set expandtab
