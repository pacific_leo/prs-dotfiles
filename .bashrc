alias ll='ls --color=auto -al'
alias rm='rm -i'
alias ls='ls --color=auto '

export PROMPT_COMMAND='__git_ps1 "\\[\\033[35m\\]\D{%H:%M} \\033k@${HOSTNAME}\\033\\ \\[\\033[33m\\]$PWD\\[\\033[36m\\]" "\\[\\033[0m\\]\\n$ "'

